import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;import java.awt.event.ComponentAdapter;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton beefBowlButton;
    private JButton friedOysterSetButton;
    private JButton gingerGrilledSetButton;
    private JButton checkOutButton;
    private JTextPane orderlist;
    private JTextPane total;

    int Sum =0;

    public FoodGUI() {
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("てんぷら.jpg")
        ));
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food="Tempura";
                int price =600;
                order(food,price);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ラーメン.jpg")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food="Ramen";
                int price =800;
                order(food,price);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("うどん.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food="Udon";
                int price =300;
                order(food,price);
            }
        });
        beefBowlButton.setIcon(new ImageIcon(
                this.getClass().getResource("牛丼.jpg")
        ));
        beefBowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food="Beef Bowl";
                int price =500;
                order(food,price);
            }
        });
        friedOysterSetButton.setIcon(new ImageIcon(
                this.getClass().getResource("牡蠣フライ.jpg")
        ));
        friedOysterSetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food="Fried Oyster Set";
                int price =1100;
                order(food,price);
            }
        });
        gingerGrilledSetButton.setIcon(new ImageIcon(
                this.getClass().getResource("生姜焼き.jpg")
        ));
        gingerGrilledSetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food="Ginger Grilled Set";
                int price =800;
                order(food,price);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                check();
            }
        });
    root.addComponentListener(new ComponentAdapter() { } );

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + " for "+price+ " yen ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            if (food == "Fried Oyster Set" || food == "Ginger Grilled Set") {
                int confirmation1 = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like a large rice for 100 yen?",
                        "Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                int confirmation2 = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to change to pork miso soup for 80 yen?",
                        "Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation1 == 0) {
                    food += "(Large Rice)";
                    price += 100;
                }
                if (confirmation2 == 0) {
                    food += "(Change to pork miso soup)";
                    price += 80;
                }
            }
            Sum += price;
            String currentText = orderlist.getText();
            orderlist.setText(currentText + food + "   " + price + "yen\n");
            total.setText("Total    " + Sum + " yen");
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for Ordering for " + food + "!");

        }
    }
        void check(){
            int pay = JOptionPane.showConfirmDialog(
                    null,
                    "Thank you! The total price is " + Sum +" yen",
                    "Pay Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if(pay == 0) {
                Sum = 0;
                String currentText = orderlist.getText();
                orderlist.setText("");
                String currentText2 = total.getText();
                total.setText("Total    "+ Sum +" yen");

            }
        }
    }
